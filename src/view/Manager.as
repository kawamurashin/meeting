/**
 * Created with IntelliJ IDEA.
 * User: jaiko
 * Date: 14/08/11
 * Time: 13:48
 * To change this template use File | Settings | File Templates.
 */
package view {
import flash.display.Sprite;
import flash.events.Event;

public class Manager extends Sprite {

    public function Manager():void {
        if (stage) init();
        else addEventListener(Event.ADDED_TO_STAGE, init);
    }

    private function init(e:Event = null):void {
        removeEventListener(Event.ADDED_TO_STAGE, init);
        // entry point
        layout();
    }

    protected function layout():void {

    }
}
}