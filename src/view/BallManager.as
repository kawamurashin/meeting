/**
 * Created with IntelliJ IDEA.
 * User: jaiko
 * Date: 14/08/11
 * Time: 17:49
 * To change this template use File | Settings | File Templates.
 */
package view {
import avmplus.factoryXml;

import flash.display.Sprite;
import flash.events.Event;

import view.ViewManager;

import view.ViewManager;

import view.ViewManager;

public class BallManager extends Sprite{
    private var _current:uint = 0;
    private var currentTarget:Sprite;
    //private var viewManager:ViewManager;
    private  var ballList:Array;
    private var targetList:Array;
    //
    public function BallManager() {
        if(stage)init(null);
        else addEventListener(Event.ADDED_TO_STAGE,init)
    }
    public  function setup(color:uint):void
    {
        var i:uint;
        var n:uint;
        var ball:Ball;
        var theta:Number;
        //

        ballList = [];
        n = 60;
        for(i=0;i<n;i++)
        {
            ball = new Ball();
            addChild(ball);
            //ball.x = 50 + (stage.stageWidth -100)*Math.random();
            //ball.y = 50 + (stage.stageHeight -100)*Math.random();
            ball.setup(color);
            ball.isMove = true;
            ball.x = stage.stageWidth * Math.random();
            ball.y = stage.stageHeight * Math.random();
            theta = 2 * Math.PI * i/n;
           // ball.targetX = currentTarget.x + ViewManager.RADIUS * Math.cos(theta);
            //ball.targetY = currentTarget.y + ViewManager.RADIUS * Math.sin(theta);
            //ball.rotation = 360 * Math.random();
            ball.theta = theta;
            ballList.push(ball);

        }


    }
    public function onEnterFrame():void
    {
        var i:uint;
        var n:uint;
        var ax:Number;
        var ay:Number;
        var d:Number;
        var k:Number;
        var u:Number
        var ball:Ball;
        var theta:Number;
        var targetX:Number;
        var targetY:Number;
        //
        n = ballList.length;
        for(i=0;i<n;i++)
        {
            ball = ballList[i];

            if(ball.isMove)
            {
                k = 0.001;
                u = 0.05;
                targetX = currentTarget.x + ViewManager.RADIUS * Math.cos(ball.theta);
                targetY = currentTarget.y + ViewManager.RADIUS * Math.sin(ball.theta);
                //targetX = currentTarget.x;
                //targetY = currentTarget.y;
                ax = k * ( targetX - ball.x);
                ay = k * ( targetY - ball.y);
                d = Math.sqrt(Math.pow(ball.x - targetX,2) + Math.pow(ball.y - targetY ,2))
                if(d < 20)
                {
                    ball.isMove = false;
                    ball.u = 0;

                }
            }
            else
            {
                k = 0.002;

                ax = k * (currentTarget.x - ball.x);
                ay = k * (currentTarget.y - ball.y);

            }
            /*
            d = Math.sqrt(Math.pow(ball.x - currentTarget.x,2) + Math.pow(ball.y - currentTarget.y ,2))

            if(d < ViewManager.RADIUS)
            {
                k = 0.01;
                u = 0;
                ax = k * (currentTarget.x - ball.x);
                ay = k * (currentTarget.y - ball.y);
            }
            else
            {
                k = 0.001;
                u = 0.05;
                ax = k * ( (currentTarget.x + ViewManager.RADIUS * Math.cos(ball.theta)) - ball.x);
                ay = k * ( (currentTarget.y + ViewManager.RADIUS * Math.sin(ball.theta)) - ball.y);

            }
            */
            //theta = Math.atan2(mouseY - ball.y , mouseX - ball.x);

            ball.vx += ax - ball.u * ball.vx;
            ball.vy += ay - ball.u * ball.vy;

            theta = Math.atan2(ball.vy,ball.vx);
            ball.rotation = theta * 180 / Math.PI;

            ball.x += ball.vx;
            ball.y += ball.vy;
        }
    }

    private function init(event:Event):void {
        removeEventListener(Event.ADDED_TO_STAGE,init)
        //
        layout();
    }

    private function layout():void {
        var viewManager:ViewManager =  ViewManager.getInstance();
        targetList = viewManager.targetList;

        currentTarget = targetList[0]

    }


    public function set current(value:uint):void {
        _current = value;
        if(_current == targetList.length)
        {
            _current = 0;
        }
        currentTarget = targetList[_current];
        //

        var i:uint;
        var n:uint;
        var v:Number;;
        var ball:Ball;
        var theta:Number;
        n = ballList.length

        for(i=0;i<n;i++)
        {
            ball = ballList[i];
            theta =2 * Math.PI * Math.random();
            v =20 * Math.random();
            ball.isMove = true;
            ball.u = 0.04 + 0.02 * Math.random();
            ball.vx = v * Math.cos(theta);
            ball.vy = v * Math.sin(theta);
            theta = 2 * Math.PI * i/n;
            //ball.targetX = currentTarget.x + ViewManager.RADIUS * Math.cos(theta);
            //ball.targetY = currentTarget.y + ViewManager.RADIUS * Math.sin(theta);
            ball.theta = theta;
        }
         /**/
    }

    public function get current():uint {
        return _current;
    }
}
}
