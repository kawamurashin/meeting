/**
 * Created with IntelliJ IDEA.
 * User: jaiko
 * Date: 14/08/11
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
package view {
import flash.display.Graphics;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.SetIntervalTimer;
import flash.utils.Timer;

public class TargetManager extends Manager {
    private var _targetList:Array;

    private var dragTarget:Sprite;
    public function TargetManager():void {
        super();
    }

    private function init(e:Event = null):void {

    }

    override protected function layout():void {
        super.layout();
        //
        var i:uint;
        var n:uint;
        var target:Sprite;
        var g:Graphics;
        //
        _targetList = [];
        n = 4;
        for(i=0;i<n;i++)
        {
            target = new Sprite();
            addChild(target);
            target.x = 50 + (stage.stageWidth - 100) * Math.random();
            target.y = 50 + (stage.stageHeight - 100) * Math.random();
            g = target.graphics;
            g.beginFill(0xCCCCCC);
            g.drawCircle(0,0,10);
            target.addEventListener(MouseEvent.MOUSE_DOWN,mouseDownHandler)
            //
            _targetList.push(target);
        }

    }

    private function mouseDownHandler(event:MouseEvent):void {

        dragTarget = Sprite(event.currentTarget);
        dragTarget.startDrag();
        stage.addEventListener(MouseEvent.MOUSE_UP,mouseUpHandler);
    }

    private function mouseUpHandler(event:MouseEvent):void {

        dragTarget.stopDrag();
        stage.removeEventListener(MouseEvent.MOUSE_UP,mouseUpHandler);
    }


    public function get targetList():Array {
        return _targetList;
    }
}
}