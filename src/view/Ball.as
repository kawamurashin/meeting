/**
 * Created with IntelliJ IDEA.
 * User: shin
 * Date: 2014/08/10
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */
package view {
import flash.display.Graphics;
import flash.display.Sprite;
import flash.events.Event;

public class Ball extends Sprite{
    public var isMove:Boolean = false;
    public var theta:Number = 0;
    /*
    public var targetX:Number = 0;
    public var targetY:Number = 0;
    */
    public var vx:Number = 0;
    public var vy:Number = 0;
    public  var u:Number = 0;
    public function Ball() {
        if(stage)init(null);
        else addEventListener(Event.ADDED_TO_STAGE, init);
    }

    private function init(event:Event):void {
        removeEventListener(Event.ADDED_TO_STAGE, init);
        //
        layout();
    }
    public function setup(color:uint):void
    {
        var g:Graphics;
        g = this.graphics;
        g.beginFill(color,1);
        g.moveTo(10 , 0);
        g.lineTo(-5,3);
        g.lineTo(-5,-3);
        g.endFill();
    }

    private function layout():void {

    }
}
}
