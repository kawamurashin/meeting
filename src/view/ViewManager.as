/**
 * Created with IntelliJ IDEA.
 * User: shin
 * Date: 2014/08/10
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 */
package view {
import avmplus.USE_ITRAITS;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.TimerEvent;
import flash.text.TextField;
import flash.utils.Timer;

import org.osmf.events.TimeEvent;

public class ViewManager extends TargetManager{
    private  var ballManagerList:Array;
    public static const RADIUS:Number = 50;
    private static var instance:ViewManager;
    public function ViewManager(block:SingletonBlock) {
          super();
    }

    public  static function getInstance():ViewManager
    {
        if(instance == null)
        {
            instance = new ViewManager(new SingletonBlock());
        }
        return instance;
    }

    override protected function layout():void {
        var textField:TextField = new TextField();
        textField.text = "Hello, World";
        addChild(textField);
        super.layout();
        //
        var colorList:Array = [0xFF0000,0x00FF00,0x0000FF];
        var i:uint;
        var n:uint;
        ballManagerList = [];
        var ballManager:BallManager

        n = 3;
        for(i=0;i<n;i++)
        {
            ballManager= new BallManager();
            addChild(ballManager);
            //ballManager.currentTarget = targetList[0]

            ballManager.setup(colorList[i]);
            ballManager.current = i;
            ballManagerList.push(ballManager);
        }


        addEventListener(Event.ENTER_FRAME, enterFrameHandler);

        var timer:Timer = new Timer(5000,0);
        timer.addEventListener(TimerEvent.TIMER, timerEventHanlder);
        timer.start();
    }

    private function timerEventHanlder(event:TimerEvent):void
    {
        /*
        var ballManager:BallManager;
        var i:uint;
        var n:uint;

        n = ballManagerList.length;
        for(i=0;i<n;i++)
        {
            ballManager = ballManagerList[i];
            ballManager.current++;
        }
        */

        var timer:Timer = new Timer(250,3);
        timer.addEventListener(TimerEvent.TIMER, countTimerEventHandler);
        timer.start();
    }

    private function countTimerEventHandler(event:TimerEvent):void {
       var timer:Timer = Timer(event.currentTarget);
        timer.currentCount;
        var ballManager:BallManager;
        ballManager = ballManagerList[timer.currentCount - 1];
        ballManager.current++;
    }

    private function enterFrameHandler(event:Event):void
    {

        var ballManager:BallManager;
        var i:uint;
        var n:uint;

        n = ballManagerList.length;
        for(i=0;i<n;i++)
        {
            ballManager = ballManagerList[i];
            ballManager.onEnterFrame();

        }

    }

    /*
    override protected function timerEventHandler(event:TimerEvent):void
    {
        super.timerEventHandler(event);
        var i:uint;
        var n:uint;
        var v:Number;;
        var ball:Ball;
        var theta:Number;
        n = ballList.length
        for(i=0;i<n;i++)
        {
            ball = ballList[i];
            theta =2 * Math.PI * Math.random();
            v =100 * Math.random();

            ball.vx = v * Math.cos(theta);
            ball.vy = v * Math.sin(theta);
            theta = 2 * Math.PI * i/n;
            ball.targetX = currentTarget.x + RADIUS * Math.cos(theta);
            ball.targetY = currentTarget.y + RADIUS * Math.sin(theta);
           // ball.rotation = theta * 180 / Math.PI;

        }
    }
    */

}
}
class SingletonBlock{

}